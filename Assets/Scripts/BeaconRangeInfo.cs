﻿public class BeaconRangeInfo
{
	public string UUID { get; private set; }
	public ushort Major { get; private set; }
	public ushort Minor { get; private set; }
	public float Accuracy { get; private set; }
	public BeaconProximity Proximity { get; private set; }
	public int RSSI { get; private set; }
	
	public BeaconRangeInfo(string uuid, ushort major, ushort minor, float accuracy,
	                       BeaconProximity proximity, int rssi)
	{
		UUID = uuid;
		Major = major;
		Minor = minor;
		Accuracy = accuracy;
		Proximity = proximity;
		RSSI = rssi;
	}
}