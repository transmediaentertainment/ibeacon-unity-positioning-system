﻿using UnityEngine;
using System.Collections;

public class iBeaconTest : MonoBehaviour
{
    public iBeacon[] m_Beacons;

	private void OnGUI()
	{
        for (int i = 0; i < m_Beacons.Length; i++)
        {
            PrintInfo(m_Beacons[i]);
        }
	}

    private void PrintInfo(iBeacon info)
	{
		GUILayout.Label("Beacon:" + info.UUID + ":" + info.Major + ":" + info.Minor);
		GUILayout.Label("Proximity:"+ info.Proximity.ToString());
		GUILayout.Label("Accuracy: " + info.Accuracy);
		GUILayout.Label("RSSI: " + info.RSSI);
		GUILayout.Label("Drops: " + info.DropCount);
	}
}
