﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class iBeaconInterface : MonoSingleton<iBeaconInterface> 
{
	public event Action<BeaconRangeInfo> NewBeaconRangeInfoEvent;

	[DllImport("__Internal")]
	private static extern void InitRegionWithUUIDAndCompanyID(string uuid, string companyID);

    public static void PluginInitRegionWithUUIDAndComapnyID(string uuid, string companyID)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            InitRegionWithUUIDAndCompanyID(uuid, companyID);
        }
    }

	[DllImport("__Internal")]
	private static extern void InitRegionWithUUIDMajorAndCompanyID(string uuid, ushort major, string companyID);

    public static void PluginInitRegionWithUUIDMajorAndCompanyID(string uuid, ushort major, string companyID)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            InitRegionWithUUIDMajorAndCompanyID(uuid, major, companyID);
        }
    }

	[DllImport("__Internal")]
	private static extern void InitRegionWithUUIDMajorMinorAndCompanyID(string uuid, ushort major, ushort minor, string companyID);

    public static void PluginInitRegionWithUUIDMajorMinorAndCompanyID(string uuid, ushort major, ushort minor, string companyID)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            InitRegionWithUUIDMajorMinorAndCompanyID(uuid, major, minor, companyID);
        }
    }

	public override void Init()
	{
        base.Init();
		name = "iBeaconMessageReceiver";
	}

#if UNITY_EDITOR
    private float m_Timer;
    private float m_Interval = 1f;
    void Update()
    {
        m_Timer += Time.deltaTime;
        if (m_Timer >= m_Interval)
        {
            m_Timer -= m_Interval;
            // Sample fake data
            for(int i = 0; i < 4; i++)
            {
                var acc =  UnityEngine.Random.Range(0f, 50f);
                BeaconProximity prox = BeaconProximity.Unknown;
                if(acc < 0.5f)
                    prox = BeaconProximity.Immediate;
                else if(acc < 2.0f)
                    prox = BeaconProximity.Near;
                else if(acc < 30f)
                    prox = BeaconProximity.Far;
                else
                    prox = BeaconProximity.Unknown;

                if(acc >= 30f)
                    acc = -1f;

                var beacon = new BeaconRangeInfo("E20A39F4-73F5-4BC4-A12F-17D1AD07A961", (ushort)0, (ushort)i, acc, prox, -59);
                if (NewBeaconRangeInfoEvent != null)
                    NewBeaconRangeInfoEvent(beacon);
            }
        }
    }
#endif

	private void DidRangeBeacon(string message)
	{
		//Debug.Log("Received Range Message : " + message);
		var splits = message.Split('|');
		ushort major;
		if(!ushort.TryParse(splits[2], out major))
		{
			Debug.LogError("Couldn't parse Major token.");
			return;
		}
		ushort minor;
		if(!ushort.TryParse(splits[3], out minor))
		{
			Debug.LogError("Couldn't parse Minor token.");
			return;
		}
		float accuracy;
		if(!float.TryParse(splits[4], out accuracy))
		{
			Debug.LogError("Couldn't parse Accuracy token");
			return;
		}
		BeaconProximity proximity = BeaconProximity.Unknown;
		switch(splits[5])
		{
		case "Immediate":
			proximity = BeaconProximity.Immediate;
			break;
		case "Near":
			proximity = BeaconProximity.Near;
			break;
		case "Far":
			proximity = BeaconProximity.Far;
			break;
		}
		int rssi;
		if(!int.TryParse(splits[6], out rssi))
		{
			Debug.LogError("Couldn't parse RSSI Token.");
			return;
		}
		if(NewBeaconRangeInfoEvent != null)
		{
			var rangeInfo = new BeaconRangeInfo(splits[1], major, minor, accuracy, proximity, rssi);
			NewBeaconRangeInfoEvent(rangeInfo);
		}
	}

}
