﻿using UnityEngine;
using System.Collections;
using System.IO;

public enum BeaconProximity
{
    Unknown,
    Immediate,
    Near,
    Far
}

public class iBeacon : MonoBehaviour
{
    [SerializeField]
    private string m_UUID;
    public string UUID { get { return m_UUID; } }
    [SerializeField]
    private int m_Major;
    public int Major { get { return m_Major; } }
    [SerializeField]
    private int m_Minor;
    public int Minor { get { return m_Minor; } }
    [SerializeField]
    private string m_Identifier;
    public string Identifier { get { return m_Identifier; } }
    [SerializeField]
    private float m_Accuracy;
    public float Accuracy { get { return m_Accuracy; } }
    [SerializeField]
    private BeaconProximity m_Proximity;
    public BeaconProximity Proximity { get { return m_Proximity; } }
    [SerializeField]
    private int m_RSSI;
    public int RSSI { get { return m_RSSI; } }
	[SerializeField]
	private Color m_BeaconColor = Color.white;

	private int m_DropCount = 0;
	public int DropCount { get { return m_DropCount; } }
	private const int m_DropLimit = 3;

	private const int m_DampingFactor = 4;

	private RangeRadius m_RangeRadius;

    private void Start()
    {
        if (string.IsNullOrEmpty(m_UUID) || !IsUShort(m_Minor) || !IsUShort(m_Major) || string.IsNullOrEmpty(m_Identifier))
        {
            // Not valid info, do nothing.
            return;
        }
		//renderer.enabled = false;
		m_RangeRadius = GetComponentInChildren<RangeRadius>();
		if(m_RangeRadius != null)
		{
			m_RangeRadius.SetColor(m_BeaconColor);
		}
		renderer.material.color = m_BeaconColor;
        Init(m_UUID, m_Major, m_Minor, m_Identifier);
    }

    public void Init(string uuid, int major, int minor, string identifier)
    {
        if(string.IsNullOrEmpty(uuid))
        {
            Debug.LogError("UUID was null or empty");
            return;
        }
        m_UUID = uuid;

        if (!IsUShort(major))
        {
            Debug.LogError("Major wasn't in ushort range : " + major);
            return;
        }
        m_Major = major;

        if (!IsUShort(minor))
        {
            Debug.LogError("Major wasn't in ushort range : " + minor);
            return;
        }
        m_Minor = minor;

        if(string.IsNullOrEmpty(identifier))
        {
            Debug.LogError("Identifier was null or empty");
            return;
        }
        m_Identifier = identifier;

        iBeaconInterface.PluginInitRegionWithUUIDMajorMinorAndCompanyID(m_UUID, (ushort)m_Major, (ushort)m_Minor, m_Identifier);
		//iBeaconInterface.PluginInitRegionWithUUIDAndComapnyID(m_UUID, m_Identifier); 
        iBeaconInterface.Instance.NewBeaconRangeInfoEvent += NewBeaconInfo;
    }

    private void OnDestroy()
    {
        if (iBeaconInterface.Exists)
        {
            iBeaconInterface.Instance.NewBeaconRangeInfoEvent -= NewBeaconInfo;
        }
    }

    private void NewBeaconInfo(BeaconRangeInfo info)
    {
        if (m_UUID == info.UUID && m_Major == info.Major && m_Minor == info.Minor)
		{
			AppendAccuracyInfoToLog(info.Accuracy);
			if(info.Accuracy > 0f)
			{
				m_DropCount = 0;
				UpdateBeaconInfo(info);
			}
			else
			{
				m_DropCount++;
				if(m_DropCount <= m_DropLimit)
				{
					UpdateBeaconInfo(info);
				}
			}
		}
	}

	private void UpdateBeaconInfo(BeaconRangeInfo info)
	{
		// Update our info
		// position = 1*current_reading/4 + 3*position/4;
		//m_Accuracy = info.Accuracy;
		m_Accuracy = (info.Accuracy / m_DampingFactor) + (((m_DampingFactor - 1) * m_Accuracy) / m_DampingFactor);
		m_Proximity = info.Proximity;
		m_RSSI = info.RSSI;
		if(m_RangeRadius != null)
			m_RangeRadius.SetNewRadius(m_Accuracy);
		DevicePositioningSystem.SetDirty();
	}
	
	private bool IsUShort(int i)
    {
        return i >= 0 || i <= 65535;
    }

	#region File Logging

	private string m_LogPath;

	private void Awake()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) 
		{
			m_LogPath = Application.dataPath.Replace ("/ProductName.app/Data", "/Documents");
		}
		else if(Application.isEditor)
		{
			m_LogPath = Application.dataPath.Replace("/Assets", "");
		}
		else
		{
			m_LogPath = Application.dataPath;
		}

		m_LogPath += "/Log" + m_Minor + ".csv";
		if(File.Exists(m_LogPath))
		{
			// So we get a new file each run through.
			File.Delete(m_LogPath);
		}
		Debug.Log(m_LogPath);
	}

	private void AppendAccuracyInfoToLog(float acc)
	{
		using(StreamWriter sw = File.AppendText(m_LogPath))
		{
			sw.Write(acc.ToString() + ",");
		}
	}

	#endregion
}
