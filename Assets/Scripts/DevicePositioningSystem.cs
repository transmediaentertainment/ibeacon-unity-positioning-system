﻿using UnityEngine;
using System.Collections;

public class DevicePositioningSystem : MonoBehaviour
{
	public static bool IsDirty { get; private set; }
	public static void SetDirty() { IsDirty = true; }

	public iBeacon[] m_Beacons;

	private void LateUpdate()
	{
		if(IsDirty)
		{
			var r1 = m_Beacons[0].Accuracy;
			var r2 = m_Beacons[1].Accuracy;
			var r3 = m_Beacons[3].Accuracy;
			var d = (m_Beacons[0].transform.position - m_Beacons[1].transform.position).magnitude;

			var ex = (m_Beacons[1].transform.position - m_Beacons[0].transform.position).normalized;

			var i = Vector3.Dot(ex, m_Beacons[3].transform.position - m_Beacons[0].transform.position);

			var ey = (m_Beacons[3].transform.position - m_Beacons[0].transform.position - (i * ex)).normalized;

			var j = Vector3.Dot(ey, (m_Beacons[3].transform.position - m_Beacons[0].transform.position));


			float x = ((r1 * r1) - (r2 * r2) + (d * d)) / (2 * d);
			float y = ((r1 * r1) - (r3 * r3) - (x * x) + ((x - i) * (x - i)) + (j * j)) / (2 * j);
			float z = Mathf.Sqrt((r1 * r1) - (x * x) - (y * y));

			Debug.Log("r1 : " + r1 + " : x : " + x + " : y : " + y);  
			Debug.Log("Setting Device position : " + (new Vector3(x, y, z)));
			if(!float.IsNaN(z))
				transform.position = new Vector3(x, y, z);

			IsDirty = false;
		}
	}
}
