﻿using UnityEngine;
using System.Collections;

public class RangeRadius : MonoBehaviour
{
	private float m_Target = 1f;
	private float m_Damping = 5f;
	private Color m_Color;

	public void SetColor(Color col)
	{
		m_Color = col;
		col.a = 0.3f;
		transform.GetChild(0).renderer.material.color = col;
	}

	public void SetNewRadius(float radius)
	{
		m_Target = radius;
	}

	private void Update()
	{
		transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(m_Target, 1f, m_Target), Time.deltaTime * m_Damping);
		var dist = transform.localScale.x;
		renderer.material.color = Color.Lerp(m_Color, Color.black, dist / 30f);

	}
}
