#import <CoreLocation/CoreLocation.h>

@interface iBeaconInterface : NSObject <CLLocationManagerDelegate>

+ (iBeaconInterface*)iBeaconInterfaceInstance;
- (void)afterDelayStopRangingBeaconsInRegion:(CLBeaconRegion*)region;

@end