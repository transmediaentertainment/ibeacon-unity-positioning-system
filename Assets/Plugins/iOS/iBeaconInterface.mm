#import "iBeaconInterface.h"

@interface iBeaconInterface ()

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *exitingBeacons;

@end

static iBeaconInterface* _iBeaconInterfaceInstance = nil;

@implementation iBeaconInterface

+(iBeaconInterface*)iBeaconInterfaceInstance
{
	@synchronized([iBeaconInterface class])
	{
		if(!_iBeaconInterfaceInstance)
			_iBeaconInterfaceInstance = [[self alloc] init];

		return _iBeaconInterfaceInstance;
	}
	return nil;
}

-(id)init 
{
	self = [super init];
	if(!self)
		return nil;

	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;

	self.exitingBeacons = [[NSMutableArray alloc] init];
	return self;
}

-(void)initRegionWithID:(NSString*)UUID forCompanyID:(NSString*)companyID
{
	NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUID];
	CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:companyID];
	[self.locationManager startMonitoringForRegion:region];
}

-(void)initRegionWithID:(NSString*)UUID major:(CLBeaconMajorValue)major forCompanyID:(NSString*)companyID
{
	NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUID];
	CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major identifier:companyID];
	[self.locationManager startMonitoringForRegion:region];
}

-(void)initRegionWithID:(NSString*)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor forCompanyID:(NSString*)companyID
{
	NSLog(@"Initing with full region : %@", UUID);
	NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUID];
	CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major minor:minor identifier:companyID];
	[self.locationManager startMonitoringForRegion:region];
}

// Delegate functions
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region 
{
	[self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
	CLBeaconRegion *region1 = (CLBeaconRegion*)region;
	for(int i = 0; i < [self.exitingBeacons count]; i++)
	{
		CLBeaconRegion *region2 = (CLBeaconRegion*)[self.exitingBeacons objectAtIndex:i];
		if([region1 isEqual:region2])
		{
			NSLog(@"Removing from delayed exiting");
			[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(afterDelayStopRangingBeaconsInRegion:) object:region1];
			[self.exitingBeacons removeObjectAtIndex:i];
			// Once we have removed from the array, we just want to exit as we are already ranging this beacon region
			return;
		}
	}

	[self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region 
{
	//[self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion*)region];

	CLBeaconRegion *region1 = (CLBeaconRegion*)region;
	for(int i = 0; i < [self.exitingBeacons count]; i++)
	{
		CLBeaconRegion *region2 = (CLBeaconRegion*)[self.exitingBeacons objectAtIndex:i];
		if([region1 isEqual:region2])
		{
			// We already have one, so restart the timer
			// In theory this shouldn't get hit, but just in case...
			NSLog(@"Hit the thing we shouldn't have!");
			[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(afterDelayStopRangingBeaconsInRegion:) object:region1];
		}
	}

	NSLog(@"Started delayed exit");
	[self performSelector:@selector(afterDelayStopRangingBeaconsInRegion:) withObject:region1 afterDelay:4];
	[self.exitingBeacons addObject:region1];
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region 
{
	if(!beacons)
	{
		NSLog(@"Beacons was null!");
		return;
	}

	for(int i = 0; i < [beacons count]; i++)
	{
		CLBeacon *beacon = [beacons objectAtIndex:i];
		if(!beacon)
		{
			NSLog(@"beacon was nil at index %i!", i);
			continue;
		}
		NSMutableString *message = [[NSMutableString alloc] initWithString:@"Range|"]; // 0
		[message appendString:beacon.proximityUUID.UUIDString]; // 1
		[message appendString:@"|"];
		[message appendString:[NSString stringWithFormat:@"%@", beacon.major]]; // 2
		[message appendString:@"|"];
		[message appendString:[NSString stringWithFormat:@"%@", beacon.minor]]; // 3
		[message appendString:@"|"];
		[message appendString:[NSString stringWithFormat:@"%f", beacon.accuracy]]; // 4
		[message appendString:@"|"];
		if (beacon.proximity == CLProximityUnknown) {
	        [message appendString:@"Unknown Proximity"]; // 5
	    } else if (beacon.proximity == CLProximityImmediate) {
	        [message appendString:@"Immediate"]; // 5
	    } else if (beacon.proximity == CLProximityNear) {
	        [message appendString:@"Near"]; // 5
	    } else if (beacon.proximity == CLProximityFar) {
	        [message appendString:@"Far"]; // 5
	    }
		
		[message appendString:@"|"];
		[message appendString:[NSString stringWithFormat:@"%li", (long)beacon.rssi]]; // 6

		UnitySendMessage("iBeaconMessageReceiver", "DidRangeBeacon", [message UTF8String]);
	}
}

- (void)afterDelayStopRangingBeaconsInRegion:(CLBeaconRegion*)region 
{
	NSLog(@"Exiting after delay");
	[self.locationManager stopRangingBeaconsInRegion:region];
}

@end

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

extern "C"
{
	void InitRegionWithUUIDAndCompanyID(const char* uuid, const char* companyID)
	{
		[[iBeaconInterface iBeaconInterfaceInstance] initRegionWithID:CreateNSString(uuid) forCompanyID:CreateNSString(companyID)];
	}

	void InitRegionWithUUIDMajorAndCompanyID(const char* uuid, uint16_t major, const char* companyID)
	{
		[[iBeaconInterface iBeaconInterfaceInstance] initRegionWithID:CreateNSString(uuid) major:(CLBeaconMajorValue)major forCompanyID:CreateNSString(companyID)];
	}

	void InitRegionWithUUIDMajorMinorAndCompanyID(const char* uuid, uint16_t major, uint16_t minor, const char* companyID)
	{
		[[iBeaconInterface iBeaconInterfaceInstance] initRegionWithID:CreateNSString(uuid) major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor forCompanyID:CreateNSString(companyID)];
	}
}

